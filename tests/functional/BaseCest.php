<?php

/**
 * Base contains test cases for tesing api endpoint 
 * 
 * @see https://codeception.com/docs/modules/Yii2
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BaseCest
{
    public static $username1 = 'kfr';
    public static $username2 = 'kft';
    public static $user1GithubNum;
    public static $user1GitlabNum;
    public static $user1BitbucketNum;
    public static $user2BitbucketNum;
    public static $emptyJson = '[]';

    function beforeAllTests(\FunctionalTester $I){
        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1,
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $json = json_decode($I->grabPageSource());
        self::$user1GithubNum = $json == json_decode(self::$emptyJson) ? 0 : count($json[0]->repo);

        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1,
            ],
            'platforms' => [
                'gitlab',
            ]
        ]);
        $json = json_decode($I->grabPageSource());
        self::$user1GitlabNum = $json == json_decode(self::$emptyJson) ? 0 : count($json[0]->repo);

        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1,
            ],
            'platforms' => [
                'bitbucket',
            ]
        ]);
        $json = json_decode($I->grabPageSource());
        self::$user1BitbucketNum = $json == json_decode(self::$emptyJson) ? 0 : count($json[0]->repo);

        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username2,
            ],
            'platforms' => [
                'bitbucket',
            ]
        ]);
        $json = json_decode($I->grabPageSource());
        self::$user2BitbucketNum = $json == json_decode(self::$emptyJson) ? 0 : count($json[0]->repo);
    }

    /**
     * Example test case
     *
     * @return void
     */
    public function cestExample(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'kfr',
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $expected = json_decode('[
            {
                "name": "kfr",
                "platform": "github",
                "total-rating": 1.5,
                "repos": [],
                "repo": [
                    {
                        "name": "kf-cli",
                        "fork-count": 0,
                        "start-count": 2,
                        "watcher-count": 2,
                        "rating": 1
                    },
                    {
                        "name": "cards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "UdaciCards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "unikgen",
                        "fork-count": 0,
                        "start-count": 1,
                        "watcher-count": 1,
                        "rating": 0.5
                    }
                ]
            }
        ]');
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api without any param
     *
     * @return void
     */
    public function cestEmptyParams(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
    }

    /**
     * Test case for api with bad request params
     *
     * @return void
     */
    public function cestBadParams(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                '%!<'
            ],
            'platforms' => [
                '<>'
            ]
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
    }

    /**
     * Test case for api with empty user list
     *
     * @return void
     */
    public function cestEmptyUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                '\'\''
            ],
            'platforms' => [
                'gitlab'
            ]
        ]);
        
        $expected = json_decode(self::$emptyJson);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with empty platform list
     *
     * @return void
     */
    public function cestEmptyPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1
            ],
            'platforms' => [
                '\'\''
            ]
        ]);
        
        $expected = json_decode(self::$emptyJson);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with non empty platform list
     *
     * @return void
     */
    public function cestSeveralPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1
            ],
            'platforms' => [
                'bitbucket', 'gitlab'
            ]
        ]);
        
        $expectedNum = self::$user1BitbucketNum + self::$user1GitlabNum;
        $json = $I->grabPageSource();
        $actualNum = json_decode($json) == null ? 0 : count($json[0]->repo);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expectedNum, $actualNum);
    }

    /**
     * Test case for api with non empty user list
     *
     * @return void
     */
    public function cestSeveralUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1, self::$username2
            ],
            'platforms' => [
                'bitbucket'
            ]
        ]);
        
        $expectedNum = self::$user1BitbucketNum + self::$user2BitbucketNum;
        $json = $I->grabPageSource();
        $actualNum = json_decode($json) == null ? 0 : count($json[0]->repo);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expectedNum, $actualNum);
    }

    /**
     * Test case for api with unknown platform in list
     *
     * @return void
     */
    public function cestUnknownPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1
            ],
            'platforms' => [
                'myhub'
            ]
        ]);
        
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
    }

    /**
     * Test case for api with unknown user in list
     *
     * @return void
     */
    public function cestUnknowUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'unknown'
            ],
            'platforms' => [
                'bitbucket'
            ]
        ]);
        
        $expected = json_decode(self::$emptyJson);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with mixed (unknown, real) users and non empty platform list
     *
     * @return void
     */
    public function cestMixedUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'unknown', self::$username1
            ],
            'platforms' => [
                'bitbucket'
            ]
        ]);
        
        $expectedNum = self::$user1BitbucketNum + self::$user2BitbucketNum;
        $json = $I->grabPageSource();
        $actualNum = json_decode($json) == null ? 0 : count($json[0]->repo);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expectedNum, $actualNum);
    }

    /**
     * Test case for api with mixed (github, gitlab, bitbucket) platforms and non empty user list
     *
     * @return void
     */
    public function cestMixedPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                self::$username1
            ],
            'platforms' => [
                'bitbucket', 'myhub'
            ]
        ]);
        
        $expectedNum = self::$user1BitbucketNum;
        $json = $I->grabPageSource();
        $actualNum = json_decode($json) == null ? 0 : count($json[0]->repo);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals($expected, $actualNum);
    }
}