<?php

namespace tests;

use app\models;

/**
 * GitlabRepoTest contains test casess for gitlab repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GitlabRepoTest extends \Codeception\Test\Unit
{
    protected $gitlabRepo;

    protected function _after()
    {
        unset($this->gitlabRepo);
    }
    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCountWhenPassNullForksShouldReturnError()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', null, 1);

        try {
            $this->gitlabRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null forks');
    }

    public function testRatingCountWhenPassNullWatchersShouldReturnError()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', 1, null);
        try {
            $this->gitlabRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null start');
    }

    public function testRatingCountWhenPassStringsShouldReturnError()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', 'forks', 'watchers');
        try {
            $this->gitlabRepo->getRating();
        }
        catch (\Exception $ex)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with strings instead of numbers');
    }

    public function testRatingCountWhenPassCorrectNumbersShouldReturnFloat()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', 1, 1);
        $rating = $this->gitlabRepo->getRating();
        $this->assertEquals(2.5/2.0, $rating);
    }

    public function testRatingCountWhenPassCorrectNumbersShouldReturnInt()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', 1, 4);
        $rating = $this->gitlabRepo->getRating();
        $this->assertEquals(2, $rating);
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testDataWhenCreateCorrectClassShouldReturnArray()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', 1, 2);
        $data = $this->gitlabRepo->getData();
        $this->assertEquals(array(
            'name' => 'myrepo',
            'fork-count' => 1,
            'start-count' => 2,
            'rating' => 1.5
        ), $data);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringifyWhenCreateCorrectShouldReturnString()
    {
        $this->gitlabRepo = new models\GitlabRepo('myrepo', 1, 2);
        $str = $this->gitlabRepo->__toString();
        $this->assertStringMatchesFormat("%s %d ⇅ %d ★", $str);
        $this->assertContains('myrepo', $str);
        $this->assertContains('1', $str);
        $this->assertContains('2', $str);
    }
}