<?php

namespace tests;

use app\models;

/**
 * GithubRepoTest contains test casess for github repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GithubRepoTest extends \Codeception\Test\Unit
{
    protected $githubRepo;

    protected function _after()
    {
        unset($this->githubRepo);
    }
    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCountWhenPassNullForksShouldReturnError()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', null, 1, 1);

        try {
            $this->githubRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null forks');
    }

    public function testRatingCountWhenPassNullStartShouldReturnError()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 1, null, 1);
        try {
            $this->githubRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null start');
    }

    public function testRatingCountWhenPassNullWatchersShouldReturnError()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 1, 1, null);
        try {
            $this->githubRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null watchers');
    }

    public function testRatingCountWhenPassStringsShouldReturnError()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 'forks', 'start', 'watchers');
        try {
            $this->githubRepo->getRating();
        }
        catch (\Exception $ex)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with strings instead of numbers');
    }

    public function testRatingCountWhenPassCorrectNumbersShouldReturnFloat()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 1, 1, 1);
        $rating = $this->githubRepo->getRating();
        $this->assertEquals(3.5/3.0, $rating);
    }

    public function testRatingCountWhenPassCorrectNumbersShouldReturnInt()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 1, 4, 2);
        $rating = $this->githubRepo->getRating();
        $this->assertEquals(2, $rating);
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testDataWhenCreateCorrectClassShouldReturnArray()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 1, 2, 3);
        $data = $this->githubRepo->getData();
        $this->assertEquals(array(
            'name' => 'myrepo',
            'fork-count' => 1,
            'start-count' => 2,
            'watcher-count' => 3,
            'rating' => 2
        ), $data);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringifyWhenCreateCorrectShouldReturnString()
    {
        $this->githubRepo = new models\GithubRepo('myrepo', 1, 2, 3);
        $str = $this->githubRepo->__toString();
        $this->assertStringMatchesFormat("%s %d ⇅ %s %d 👁️", $str);
        $this->assertContains('myrepo', $str);
        $this->assertContains('1', $str);
        $this->assertContains('2', $str);
        $this->assertContains('3', $str);
    }
}