<?php

namespace tests;

use app\models;

/**
 * BitbucketRepoTest contains test casess for bitbucket repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BitbucketRepoTest extends \Codeception\Test\Unit
{
    protected $bitbucketRepo;

    protected function _after()
    {
        unset($this->bitbucketRepo);
    }
    /**
     * Test case for counting repo rating
     *
     * @return void
     */
    public function testRatingCountWhenPassNullForksShouldReturnError()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', null, 1);

        try {
            $this->bitbucketRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null forks');
    }

    public function testRatingCountWhenPassNullWatchersShouldReturnError()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', 1, null);
        try {
            $this->bitbucketRepo->getRating();
        }
        catch (\Exception $e)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with null watchers');
    }

    public function testRatingCountWhenPassStringsShouldReturnError()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', 'forks', 'watchers');
        try {
            $this->bitbucketRepo->getRating();
        }
        catch (\Exception $ex)
        {
            return;
        }
        $this->fail('It\'s possible to create repo with strings instead of numbers');
    }

    public function testRatingCountWhenPassCorrectNumbersShouldReturnFloat()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', 1, 1);
        $rating = $this->bitbucketRepo->getRating();
        $this->assertEquals(1.5, $rating);
    }

    public function testRatingCountWhenPassCorrectNumbersShouldReturnInt()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', 1, 2);
        $rating = $this->bitbucketRepo->getRating();
        $this->assertEquals(2, $rating);
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     */
    public function testDataWhenCreateCorrectClassShouldReturnArray()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', 1, 2);
        $data = $this->bitbucketRepo->getData();
        $this->assertEquals(array(
            'name' => 'myrepo',
            'fork-count' => 1,
            'watcher-count' => 2,
            'rating' => 2
        ), $data);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     */
    public function testStringifyWhenCreateCorrectShouldReturnString()
    {
        $this->bitbucketRepo = new models\BitbucketRepo('myrepo', 1, 2);
        $str = $this->bitbucketRepo->__toString();
        $this->assertStringMatchesFormat("%s %d ⇅ %s %d 👁️", $str);
        $this->assertContains('myrepo', $str);
        $this->assertContains('1', $str);
        $this->assertContains('2', $str);
    }
}