<?php

namespace tests;

use app\components\platforms\Github;
use app\models\BitbucketRepo;
use app\models\GithubRepo;
use app\models\GitlabRepo;
use app\models\User;

/**
 * UserTest contains test casess for user model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class UserTest extends \Codeception\Test\Unit
{
    protected $user;

    protected function _before()
    {
        $this->user = new User('1', 'myname', 'github');
    }

    protected function _after()
    {
        unset($this->user);
    }
    /**
     * Test case for adding repo models to user model
     * 
     * @return void
     */
    public function testAddingReposWhenAddNullShouldReturnError()
    {
        try {
            $this->user->addRepos(null);
        }
        catch (\Error $e)
        {
            $this->assertInstanceOf(\TypeError::class, $e);
            return;
        }
        $this->fail('There is no error on add null repo operation');
    }

    public function testAddingReposWhenAddSomeShouldAddElements()
    {
        $githubRepo = new GithubRepo('myhub', 1, 2, 3);
        $gitlabRepo = new GitlabRepo('myhub2', 1, 2);
        $bitbucketRepo = new BitbucketRepo('myhub3', 1, 2);
        $repos = [$githubRepo, $gitlabRepo, $bitbucketRepo];

        $this->user->addRepos($repos);
        $this->assertEquals(3, count($this->user->getData()['repos']));
    }

    public function testAddingReposWhenAddSameShouldReturnTheSameArray()
    {
        $githubRepo = new GithubRepo('myhub', 1, 2, 3);
        $repos = [$githubRepo];
        $this->user->addRepos($repos);
        $repoCount = count($this->user->getData()['repos']);

        $this->user->addRepos($repos);
        $this->assertEquals($repoCount, count($this->user->getData()['repos']));
    }

    public function testAddingReposWhenAddAnotherRepoShouldReturnTheArrayWithNewRepo()
    {
        $githubRepo = new GithubRepo('myhub', 1, 2, 3);
        $bitbucketRepo = new BitbucketRepo('myhub2', 1, 2);
        $repos = [$githubRepo];
        $this->user->addRepos($repos);
        $repoCount = count($this->user->getData()['repos']);

        $this->user->addRepos([$bitbucketRepo]);
        $this->assertEquals($repoCount + 1, count($this->user->getData()['repos']));
    }

    /**
     * Test case for counting total user rating
     *
     * @return void
     */
    public function testGetRatingCountWhenHave3ReposShouldReturmSummOfTheirRetings()
    {
        $githubRepo = new GithubRepo('myhub', 1, 1, 1);
        $gitlabRepo = new GitlabRepo('myhub2', 1, 1);
        $bitbucketRepo = new BitbucketRepo('myhub3', 1, 1);
        $repos = [$githubRepo, $gitlabRepo, $bitbucketRepo];

        $this->user->addRepos($repos);
        $this->assertEquals(3.5/3.0 + 2.5/2.0 + 3.0/2.0, $this->user->getTotalRating());
    }

    /**
     * Test case for user model data serialization
     *
     * @return void
     */
    public function testDataWhenGetDataShouldReturnCorrectArray()
    {
        $githubRepo = new GithubRepo('myhub', 1, 1, 1);
        $gitlabRepo = new GitlabRepo('myhub2', 1, 1);
        $bitbucketRepo = new BitbucketRepo('myhub3', 1, 1);
        $repos = [$githubRepo, $gitlabRepo, $bitbucketRepo];
        $this->user->addRepos($repos);

        $data = $this->user->getData();

        $this->assertArraySubset(array('name' => 'myname',
            'platform' => 'github',
            'total-rating' => 3.5/3.0 + 2.5/2.0 + 3.0/2.0), $data);
        foreach ($repos as $repo)
        {
            $this->assertContains($repo->getData(), $data['repos']);
        }
    }

    /**
     * Test case for user model __toString verification
     *
     * @return void
     */
    public function testStringifyShouldReturnAllInfo()
    {
        $githubRepo = new GithubRepo('myhub', 1, 1, 1);
        $gitlabRepo = new GitlabRepo('myhub2', 1, 1);
        $bitbucketRepo = new BitbucketRepo('myhub3', 1, 1);
        $repos = [$githubRepo, $gitlabRepo, $bitbucketRepo];
        $this->user->addRepos($repos);
        $str = $this->user->__toString();

        $this->assertContains('myname (github)', $str);
        foreach ($repos as $repo)
        {
            $this->assertContains((string)$repo, $str);
        }
    }
}