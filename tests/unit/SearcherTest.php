<?php

namespace tests;

use app\components;
use app\components\platforms\Bitbucket;
use app\components\platforms\Github;
use app\components\platforms\Gitlab;


/**
 * SearcherTest contains test casess for searcher component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class SearcherTest extends \Codeception\Test\Unit
{
    protected $searcher;
    protected $username1;
    protected $username2;

    protected function _before()
    {
        $this->username1 = 'kfr';
        $this->username2 = 'kft';
        $this->searcher = new components\Searcher();
    }
    protected function _after()
    {
        unset($this->searcher);
    }

    /**
     * Test case for searching via several platforms
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testSearcherWhenPassNullPlatformsShouldReturnError()
    {
        try {
            $this->searcher->search([null], [$this->username1]);
        }
        catch (\Error $er)
        {
            return;
        }
        $this->fail('There\'s no exception on [null] platforms');
    }

    /**
     * @return void
     */
    public function testSearcherWhenPassNullUsersShouldReturnError()
    {
        try {
            $this->searcher->search([new Github([])], [null]);
        }
        catch (\Error $er)
        {
            return;
        }
        $this->fail('There\'s no exception on [null] users');
    }

    /**
     * @return void
     */
    public function testSearcherWhenPassPlatformsShouldReturnAllOptions()
    {
        $platforms = array(new Github([]), new Gitlab([]), new Bitbucket([]));
        $users = $this->searcher->search($platforms, [$this->username1]);

        $this->assertNotNull($users);
    }

    /**
     * @return void
     */
    public function testSearcherWhenPassUnexistingPlatformsShouldReturnError()
    {
        try {
            $this->searcher->search(['myhub'], [$this->username1]);
        }
        catch (\Error $er)
        {
            return;
        }
        $this->fail('Could find users of unexisting platform');
    }

    /**
     * @return void
     */
    public function testSearcherWhenPassUnexistingUserShouldReturnEmptyArray()
    {
        $users = $this->searcher->search([new Github([])], ['unexisting']);

        $this->assertCount(0, $users);
    }

    /**
     * @return void
     */
    public function testSearcherWhenPassUserWithReposShouldReturnArray()
    {
        $users = $this->searcher->search([new Github([])], [$this->username1]);

        $this->assertCount(1, $users);
    }

    /**
     * @return void
     */
    public function testSearcherWhenPassUsersArrayShouldReturnArray()
    {
        $users = $this->searcher->search([new Github([])], [$this->username1, $this->username2]);

        $this->assertNotCount(0, $users);
    }
}