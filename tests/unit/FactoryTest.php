<?php

namespace tests;

use app\components;
use app\components\platforms\Bitbucket;
use app\components\platforms\Github;
use app\components\platforms\Gitlab;

/**
 * FactoryTest contains test casess for factory component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class FactoryTest extends \Codeception\Test\Unit
{
    protected $factory;

    protected function _before()
    {
        $this->factory = new components\Factory();
    }
    protected function _after()
    {
        unset($this->factory);
    }

    /**
     * Test case for creating github component
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testCreateGithubShouldReturnGithub()
    {
        $github = $this->factory->create('github');
        $this->assertInstanceOf(Github::class, $github);
    }

    /**
     * Test case for creating gitlab component
     *
     * @return void
     */
    public function testCreateGitlabShouldReturnGitlab()
    {
        $gitlab = $this->factory->create('gitlab');
        $this->assertInstanceOf(Gitlab::class, $gitlab);
    }

    /**
     * Test case for creating bitbucket component
     *
     * @return void
     */
    public function testCreateBitBucketShouldReturnBitbucket()
    {
        $bitbucket = $this->factory->create('bitbucket');
        $this->assertInstanceOf(Bitbucket::class, $bitbucket);
    }

    /**
     * Test case for creating unknown component
     *
     * @return void
     */
    public function testCreateUnknownShouldReturnException()
    {
        try
        {
            $this->factory->create('myhub');
        }
        catch (\LogicException $le)
        {
            $this->assertNotNull($le);
            return;
        }
        $this->fail("Unknown factory created successfully");
    }
}